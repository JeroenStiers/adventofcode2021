import re
from typing import List, Dict, Set
from itertools import combinations, permutations
from util import read_input

EXPECTED_WIRING = [
    {'a', 'b', 'c', 'e', 'f', 'g'},
    {'c', 'f'},
    {'a', 'c', 'd', 'e', 'g'},
    {'a', 'c', 'd', 'f', 'g'},
    {'b', 'c', 'd', 'f'},
    {'a', 'b', 'd', 'f', 'g'},
    {'a', 'b', 'd', 'e', 'f', 'g'},
    {'a', 'c', 'f'},
    {'a', 'b', 'c', 'd', 'e', 'f', 'g'},
    {'a', 'b', 'c', 'd', 'f', 'g'}
]

EXPECTED_WIRING_PER_LENGTH = {}
for wiring in EXPECTED_WIRING:
    if len(wiring) not in EXPECTED_WIRING_PER_LENGTH:
        EXPECTED_WIRING_PER_LENGTH[len(wiring)] = set()
    EXPECTED_WIRING_PER_LENGTH[len(wiring)] = EXPECTED_WIRING_PER_LENGTH[len(wiring)].union(wiring)


def update_mapping(mapping: Dict[str, Set[str]], signal: str):

    # For each of the encodings having the same length, remove all
    # connections that are no longer possible from the mapping table

    # expected are the expected wiring connections
    # observed are the provided wiring connections
    signal = set([i for i in signal])
    expected_options = set()
    for e in EXPECTED_WIRING:
        if len(e) == len(signal):
            expected_options = expected_options.union(e)

    for s in signal:
        mapping[s] = mapping[s].intersection(expected_options)


def initialise_mapping() -> Dict[str, Set[str]]:
    options = {'a','b','c','d','e','f','g'}

    to_return = {}
    for char in options:
        to_return[char] = options.copy()
    return to_return


def analyse_wire_positions2(signal_patterns: List[str]):
    mapping = initialise_mapping()

    observed_wiring_per_length = {}
    for length in EXPECTED_WIRING_PER_LENGTH.keys():
        observed = set()
        for signal in signal_patterns:
            if len(signal) == length:
                observed = observed.union(set([i for i in signal]))
        observed_wiring_per_length[length] = observed


    # Now that we have the expected and the observed wiring combinations stored per
    # length, we will compare the differences in order to limit the problem
    lengths_present = set(EXPECTED_WIRING_PER_LENGTH.keys()).intersection(set(observed_wiring_per_length.keys()))

    # Start with comparing the same length values over the expected and the observed.
    for length in lengths_present:
        for observed in observed_wiring_per_length[length]:
            mapping[observed] = mapping[observed].intersection(EXPECTED_WIRING_PER_LENGTH[length])
    """
    Find the different between the provided lengths. Based on the differences between the 
    lengths, we will be able to limit the mapping table 
    
    e.g. 
        observed:   dab & eafb      > difference >      d
        expected:   acf & bcdf      > difference >      a
        So if we observe an 'd', we know it should be an 'a'
    """

    for count_combinations in range(2, 10):
        for p in permutations(lengths_present, count_combinations):
            difference_observed = observed_wiring_per_length[p[0]].copy()
            intersection_observed = observed_wiring_per_length[p[0]].copy()
            difference_expected = EXPECTED_WIRING_PER_LENGTH[p[0]].copy()
            intersection_expected = EXPECTED_WIRING_PER_LENGTH[p[0]].copy()

            # Only keep the differences for every next level
            for index in list(p)[1:]:
                difference_observed = difference_observed.difference(observed_wiring_per_length[index])
                intersection_observed = difference_observed.intersection(observed_wiring_per_length[index])
                difference_expected = difference_expected.difference(EXPECTED_WIRING_PER_LENGTH[index])
                intersection_expected = difference_expected.intersection(EXPECTED_WIRING_PER_LENGTH[index])

            # Use this difference to update the mapping table
            for observed in difference_observed:
                # mapping[observed] = mapping[observed].intersection(difference_expected)
                pass

            # Use this difference to update the mapping table
            for observed in intersection_observed:
                # mapping[observed] = mapping[observed].intersection(intersection_expected)
                pass


    """
    Extra mapping cleaning. We can do more with the characters for length 5 and 6. By comparing 
    the number of intersecting segments with a defined number (length 2, 3, 4 or 8) or even with 
    an undefined number (comparing length 5 and 6). We can define what number a specific length 5 / 6 
    signal is refering to. Knowing the number, we achieve more information about the wiring configuration
    
    e.g. 
        For the signals of length 5 (numbers 2, 3, 5), only one of these has got 2 intersecting 
        segments with number 1. If we find a signal of length 5 that has got 2 intersecting segments,
        with the provided signal of length 2 (that we know is number 1), we have therefore uniquely
        categorised this number and can use the information about the non-matching segments to limit 
        the amount of possibile wiring configurations.  
    """
    known_configurations = [
        {
            'length_unknown': 5,
            'length_known': 2,
            'intersecting_segments': 2,
            'unknown_number': 3,
            'known_number': 1
        },
        {
            'length_unknown': 5,
            'length_known': 3,
            'intersecting_segments': 3,
            'unknown_number': 3,
            'known_number': 7
        },
        {
            'length_unknown': 5,
            'length_known': 4,
            'intersecting_segments': 2,
            'unknown_number': 2,
            'known_number': 4
        },
        {
            'length_unknown': 6,
            'length_known': 4,
            'intersecting_segments': 4,
            'unknown_number': 9,
            'known_number': 4
        },
        {
            'length_unknown': 5,
            'length_known': 6,
            'intersecting_segments': 5,
            'unknown_number': 3,
            'known_number': 9
        }
    ]

    for config in known_configurations:
        # Check if the known value is present in the current input
        known_number = [s for s in signal_patterns if len(s) == config['length_known']]

        for signal in [s for s in signal_patterns if len(s) == config['length_unknown']]:
            for kn in known_number:
                if len(set([i for i in signal]).intersection(set([i for i in kn]))) == config['intersecting_segments']:
                    # We now know that we have a match and have therefore uniquely defined the
                    # number this signal is referring to. Let's use this info to update our mapping.
                    difference_observed = set([i for i in signal]).difference(set([i for i in kn]))
                    difference_expected = EXPECTED_WIRING[config['unknown_number']].difference(EXPECTED_WIRING[config['known_number']])

                    for observed in difference_observed:
                        mapping[observed] = mapping[observed].intersection(difference_expected)

                    if config['unknown_number'] == 2:
                         mapping[list(set([i for i in signal]).intersection(set([s for s in signal_patterns if len(s) == 2][0])))[0]] = {'c'}

    # Go through the mapping and remove the known conversions
    for observed1 in mapping:
        if len(mapping[observed1]) > 1:
            continue
        for observed2 in mapping:
            if observed1 == observed2:
                continue
            mapping[observed2] = mapping[observed2].difference(mapping[observed1])
    return mapping


def analyse_wire_positions(signal_patterns: List[str]):
    mapping = initialise_mapping()
    for signal in signal_patterns:
        update_mapping(mapping, signal)

    for key in mapping:
        print(f"{key} : {mapping[key]}")

    return mapping


if __name__ == '__main__':
    input = read_input('example.txt')

    pattern = re.compile(r'([a-z]+)')

    count = 0
    output_values = []
    for i in input:
        unique_single_patterns = pattern.findall(i.split('|')[0])
        four_digit_value = pattern.findall((i.split('|')[1]))

        for dv in four_digit_value:
            if len(dv) in [2, 4, 3, 7]:
                count += 1

        combined = unique_single_patterns.copy()
        combined.extend(four_digit_value.copy())
        mapping = analyse_wire_positions2(unique_single_patterns)
        for map in mapping:
            # print(f"{map} > {mapping[map]}")
            pass

        result = ''
        for value in four_digit_value:
            options = set()
            for char in value:
                options = options.union(mapping[char])

            for number, wiring in enumerate(EXPECTED_WIRING):
                if options == wiring:
                    result += str(number)
        if len(result) < 4:
            print(unique_single_patterns)
            for map in mapping:
                print(f"{map} > {mapping[map]}")
            print(f"{four_digit_value} > {result}")
        output_values.append(int(result))

    print(output_values)
    print(sum(output_values))

