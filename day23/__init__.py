from typing import List, Tuple, Dict, Optional, TypeVar, Set
from dataclasses import dataclass
from copy import deepcopy
from abc import ABC
import heapq
from math import inf


@dataclass
class Position:
    row: int
    col: int

    def __eq__(self, other: 'Position'):
        return self.row == other.row and self.col == other.col

    def __str__(self):
        return f"<POSITION | {self.row}, {self.col}>"

    def __hash__(self):
        return hash(str(self))

    def get_neighbours(self) -> Set['Position']:
        return {
            Position(self.row - 1, self.col),
            Position(self.row + 1, self.col),
            Position(self.row, self.col - 1),
            Position(self.row, self.col + 1)
        }


class Amphipod(ABC):
    ROOM_INDEX: int

    def __init__(self, position: Position):
        self.position: Position = position
        self.energy_per_step: int = 0
        self.energy_spent: int = 0

    def __eq__(self, other):
        return self.position == other.position and self.__class__ == other.__class__ and self.energy_spent == other.energy_spent

    def __repr__(self):
        return f"<{str(self)} | {self.position} ({self.energy_spent} energy spent)>"

    def move(self, to: Position, required_energy: int) -> None:
        self.energy_spent += required_energy * self.energy_per_step
        self.position = to


class Amber(Amphipod):
    ROOM_INDEX: int = 0

    def __init__(self, position: Position):
        super().__init__(position)
        self.energy_per_step = 1

    def __str__(self):
        return 'A'


class Bronze(Amphipod):
    ROOM_INDEX: int = 1

    def __init__(self, position: Position):
        super().__init__(position)
        self.energy_per_step = 10

    def __str__(self):
        return 'B'


class Copper(Amphipod):
    ROOM_INDEX: int = 2

    def __init__(self, position: Position):
        super().__init__(position)
        self.energy_per_step = 100

    def __str__(self):
        return 'C'


class Desert(Amphipod):
    ROOM_INDEX: int = 3

    def __init__(self, position: Position):
        super().__init__(position)
        self.energy_per_step = 1000

    def __str__(self):
        return 'D'


class Grid:

    def __init__(self, hallway: Tuple[Position, Position], rooms: List[List[Position]], amphipods: List[Amphipod]):
        self.hallway: Set[Position] = set(
            [Position(hallway[0].row, i) for i in range(hallway[0].col, hallway[1].col + 1)])
        self.rooms: List[List[Position]] = rooms
        self.entrances: Set[Position] = set(
            [pos for pos in self.hallway if pos.col in [c.col for c in self.get_positions_chamber()]])
        self.amphipods: List[Amphipod] = amphipods
        self.size: Dict[str, int] = {'rows': len(self.rooms[0]) + 1, 'cols': len(self.hallway)}

    def __str__(self):
        print(' '.join(['# ' for _ in range(self.size['cols'] + 2)]))
        for r in range(self.size['rows']):
            print('# ', end='')
            for c in range(self.size['cols']):
                position = Position(r, c)
                if a := self.get_amphipod_on_position(position):
                    print(f" {a} ", end='')
                elif position in self.entrances:
                    print(' . ', end='')
                elif position in self.hallway or position in self.get_positions_chamber():
                    print(' _ ', end='')
                else:
                    print(' # ', end='')
            print(' #')
        print(' '.join(['# ' for _ in range(self.size['cols'] + 2)]), end='')
        return ''

    def get_positions_chamber(self) -> Set[Position]:
        return set([chamber for room in self.rooms for chamber in room])

    def get_amphipod_on_position(self, position: Position):
        for a in self.amphipods:
            if a.position == position:
                return a
        return False

    def amphipod_in_correct_room(self, amphipod: Amphipod) -> bool:
        """The check for the position.row > 0 is not strictly necessary"""
        return amphipod.position.col == self.rooms[amphipod.ROOM_INDEX][0].col

    def amphipod_is_blocking(self, amphipod: Amphipod) -> bool:
        """Check if this amphipod is blocking another one to leave the room
        This function assumes that this amphipod is in it's destination room"""
        for chamber in self.rooms[amphipod.ROOM_INDEX]:
            if chamber.row > amphipod.position.row and (a := self.get_amphipod_on_position(chamber)):
                # If this chamber is further from the door than the location of this amphipod
                # and there is another amphipod in this location
                if a.ROOM_INDEX != amphipod.ROOM_INDEX:
                    # If this is an amphipod not in the destination room, it means that this amphipod is blocking
                    # another one.
                    return True
        return False

    def amphipod_is_in_hallway(self, amphipod: Amphipod) -> bool:
        return amphipod.position in self.hallway

    def get_reachable_positions(self, amphipod: Amphipod) -> List[Tuple[Position, int]]:
        """Get the positions this amphipod can get to based on the logic
        (from a room to the hallway and then to the correct room"""
        passable_positions, distances = self.get_passable_positions(position=deepcopy(amphipod.position), positions=set(), current_distance=0, distances={})

        # Remove the entrances from the passable positions since an amphipod is not allowed to stop here
        positions = passable_positions.difference(self.entrances)

        # If the resulting room for this amphipod contains other amphipods that are not in their destination room,
        # this amphipod will not enter his/her room
        can_enter_destination: bool = True
        for room_index, room in enumerate(self.rooms):
            if room_index != amphipod.ROOM_INDEX:
                # Remove the rooms that are not the destination of this Amphipod
                positions = positions.difference(room)
                continue

            # If this room is the destination, check if there is an amphipod present for which it is not it's
            # destination, in that case, remove all positions of this room.
            for chamber in room:
                if a := self.get_amphipod_on_position(chamber):
                    if a.ROOM_INDEX != amphipod.ROOM_INDEX:
                        positions = positions.difference(set(self.rooms[amphipod.ROOM_INDEX]))
                        can_enter_destination = False
                        break

        # If this amphipod is in the hallway, the next position can only be in the room so we will remove all hallway
        # positions as possibilities.
        # If the amphipod can enter the room, discard all the other options and only return the furthest position in the room
        if self.amphipod_is_in_hallway(amphipod=amphipod) or can_enter_destination:
            positions = list(positions.difference(self.hallway))
            positions = sorted(positions, key=lambda x: x.row, reverse=True)
            positions = set(positions[:1])

        to_return: List[Tuple[Position, int]] = []
        for position in positions:
            to_return.append((position, distances[position]))
            # Sort reverse since the change of finding a solution is bigger - it will have a worse energy level
            # but having something will limit the number of options
        to_return = sorted(to_return, key=lambda x: x[1], reverse=True)
        return to_return

    def get_passable_positions(self, position: Position, positions: Set[Position], current_distance: int, distances: Dict[Position, int]) -> Tuple[Set[Position], Dict[Position, int]]:
        """Return all passable positions. These are reachable, not following the set rules"""

        # Remove the options that are already analysed
        new_options = position.get_neighbours().difference(positions)

        # Only keep the valid options for which we add them to the list of positions and call the function with
        # the updated position
        current_distance += 1
        for option in filter(self.is_passable, new_options):
            if option not in positions:
                positions.add(option)
                distances[option] = current_distance
                self.get_passable_positions(position=option, positions=positions, current_distance=current_distance, distances=distances)

        return positions, distances

    def is_passable(self, position: Position):
        """Validates if the position is passable by a unit (hallway / room) where no unit is present"""
        if position not in self.hallway and position not in self.get_positions_chamber():
            return False
        if self.get_amphipod_on_position(position):
            return False
        return True


class Solver:

    def __init__(self, grid: Grid, amphipods: List[Amphipod]):
        self.grid = grid
        self.amphipods = amphipods

    def is_solved(self) -> bool:
        """Checks if all amphipods are in the correct room"""
        return sum([0 if self.grid.amphipod_in_correct_room(a) else 1 for a in self.grid.amphipods]) == 0

    def calculate_total_energy(self) -> int:
        return sum([a.energy_spent for a in self.grid.amphipods])

    def find_amphipod(self, amphipod: Amphipod):
        for a in self.amphipods:
            if a == amphipod:
                return a

    def get_amphipods_that_can_move(self) -> List[Tuple[Amphipod, List[Tuple[Position, int]]]]:
        to_return: List[Tuple[Amphipod, List[Tuple[Position, int]]]] = []
        for a in self.amphipods:

            if self.grid.amphipod_in_correct_room(a) and not self.grid.amphipod_is_blocking(a):
                continue

            positions = self.grid.get_reachable_positions(a)
            if len(positions) > 0:
                to_return.append((a, positions))
        return to_return

    def calculate_minimal_energy_required(self) -> int:
        to_return: int = 0
        for a in self.amphipods:
            if not self.grid.amphipod_in_correct_room(a):
                to_return += (abs(a.position.col - self.grid.rooms[a.ROOM_INDEX][0].col) + a.position.row) * a.energy_per_step
        return to_return

    def solve(self, required_energy: Set[int], best_positions: Dict[str, int]):

        pos1 = self.grid.get_amphipod_on_position(Position(0, 10))
        pos2 = self.grid.get_amphipod_on_position(Position(0, 0))

        if isinstance(pos1, Desert) and isinstance(pos2, Amber):
            print(self.grid)

        if self.is_solved():
            if self.calculate_total_energy() not in required_energy: # and self.calculate_total_energy() < 14500:
                print(self.calculate_total_energy())
            required_energy.add(self.calculate_total_energy())
            return

        state_hash = '+'.join([a.__repr__() for a in self.amphipods])
        if state_hash not in best_positions or best_positions[state_hash] > self.calculate_total_energy():
            best_positions[state_hash] = self.calculate_total_energy()
        elif best_positions[state_hash] <= self.calculate_total_energy():
            return

        if len(required_energy) > 0 and (self.calculate_total_energy() + self.calculate_minimal_energy_required()) > min(min(required_energy), 44200):
            # Stop with calculating if the current total required energy bigger is than the current smallest number
            return

        for a, positions in self.get_amphipods_that_can_move():
            for position, required_steps in positions:
                new_solver = deepcopy(self)
                amphipod = new_solver.find_amphipod(a)
                amphipod.move(position, required_steps)
                new_solver.solve(required_energy=required_energy, best_positions=best_positions)
        return required_energy


if __name__ == '__main__':

    """
    # DEFINE A TESTING SITUATION
    amphipods: List[Amphipod] = [
        #Bronze(Position(1, 1)),
        Amber(Position(1, 3)),
        Copper(Position(2, 3)),
        Amber(Position(1, 5))
    ]
    grid = Grid(
        hallway=(Position(0, 0), Position(0, 6)),
        rooms=[
            [Position(1, 1), Position(2, 1)],
            [Position(1, 3), Position(2, 3)],
            [Position(1, 5), Position(2, 5)]
        ],
        amphipods=amphipods
    )

    print(grid)

    s = Solver(
        grid=grid,
        amphipods=amphipods
    )
    #energy = s.solve(required_energy=set(), best_positions={})
    #print(energy)
    #print(min(energy))
    """


    # THE REAL INPUT
    amphipods: List[Amphipod] = [
        Bronze(Position(1, 2)),
        Desert(Position(2, 2)),
        Desert(Position(3, 2)),
        Copper(Position(4, 2)),
        Copper(Position(1, 4)),
        Copper(Position(2, 4)),
        Bronze(Position(3, 4)),
        Desert(Position(4, 4)),
        Amber(Position(1, 6)),
        Bronze(Position(2, 6)),
        Amber(Position(3, 6)),
        Desert(Position(4, 6)),
        Bronze(Position(1, 8)),
        Amber(Position(2, 8)),
        Copper(Position(3, 8)),
        Amber(Position(4, 8))
    ]

    """
    # THE EXAMPLE INPUT
    amphipods: List[Amphipod] = [
        Bronze(Position(1, 2)),
        Desert(Position(2, 2)),
        Desert(Position(3, 2)),
        Amber(Position(4, 2)),
        Copper(Position(1, 4)),
        Copper(Position(2, 4)),
        Bronze(Position(3, 4)),
        Desert(Position(4, 4)),
        Bronze(Position(1, 6)),
        Bronze(Position(2, 6)),
        Amber(Position(3, 6)),
        Copper(Position(4, 6)),
        Desert(Position(1, 8)),
        Amber(Position(2, 8)),
        Copper(Position(3, 8)),
        Amber(Position(4, 8))
    ]"""

    grid = Grid(
        hallway=(Position(0, 0), Position(0, 10)),
        rooms=[
            [Position(1, 2), Position(2, 2), Position(3, 2), Position(4, 2)],
            [Position(1, 4), Position(2, 4), Position(3, 4), Position(4, 4)],
            [Position(1, 6), Position(2, 6), Position(3, 6), Position(4, 6)],
            [Position(1, 8), Position(2, 8), Position(3, 8), Position(4, 8)]
        ],
        amphipods=amphipods
    )

    print(grid)
    s = Solver(
        grid=grid,
        amphipods=amphipods
    )
    energy = s.solve(required_energy=set(), best_positions={})
    print(energy)
    print(min(energy))
