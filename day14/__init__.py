from enum import Enum
from typing import Set, Dict, List
from dataclasses import dataclass
from collections import Counter
from util import read_input


class Polymer:

    def __init__(self, state: str):
        self.state: Dict[str, int] = {}
        self.insertion_rules: Dict[str, str] = {}
        self.last_char: str = ''
        self.define_initial_state(state)

    def add_rule(self, rule: str):
        combination, to_add = rule.split(' -> ')
        self.insertion_rules[combination] = to_add

    def define_initial_state(self, state: str) -> Dict[str, int]:
        for char1, char2 in zip(state[:-1], state[1:]):
            combination = f"{char1}{char2}"
            if combination not in self.state:
                self.state[combination] = 0
            self.state[combination] += 1
        self.last_char = char2

    def simulate_step(self):
        new_state: Dict[str, int] = {}
        for combination in self.state:

            if combination not in self.insertion_rules:
                self.store_combination(combination, new_state)
                new_state[combination] += self.state[combination]
                continue

            # When this is a combination that is part of the insertion_rules
            inserted_char = self.insertion_rules[combination]
            new_combination = f"{combination[0]}{inserted_char}"
            self.store_combination(new_combination, new_state)
            new_state[new_combination] += self.state[combination]

            new_combination = f"{inserted_char}{combination[1]}"
            self.store_combination(new_combination, new_state)
            new_state[new_combination] += self.state[combination]

        self.state = new_state

    def count_chars(self) -> Counter:
        counter = Counter()

        for combination in self.state:
            extra_count = Counter({combination[0]: self.state[combination]})
            counter += extra_count

        # Make sure we add the final character
        final_char_count = Counter({self.last_char: 1})
        counter += final_char_count
        return counter

    @staticmethod
    def store_combination(combination: str, state: Dict[str, int]):
        if combination not in state:
            state[combination] = 0


if __name__ == '__main__':
    input = read_input('input.txt')

    # Create the polymer
    polymer = Polymer(input.pop(0))

    input.pop(0)
    # Build the pair_insertion_rules
    try:
        while rule := input.pop(0):
            polymer.add_rule(rule)
    except IndexError:
        pass

    # Process steps
    for step in range(40):
        polymer.simulate_step()

    # Print the requested result
    counts = polymer.count_chars().items()
    counts = sorted(counts, key=lambda x: x[1])
    print(f"RESULT | {counts[-1][1] - counts[0][1]}")









