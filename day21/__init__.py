from typing import List, Tuple, Dict, Optional
from util import read_input
from copy import deepcopy
from itertools import cycle


class Cycle:

    def __init__(self, iterable):
        self.iterable = cycle(iterable)

    def get_value(self, steps: int = 1):
        value = None
        for _ in range(steps):
            value = next(self.iterable)
        return value


class DeterministicDie(Cycle):

    def __init__(self):
        super().__init__(range(1, 101))
        self.value: Optional[int] = None
        self.number_of_rolls: int = 0

    def __repr__(self):
        return f"<DETERMINISTICDICE | {self.value}, {self.number_of_rolls}>"

    def roll(self) -> 'DeterministicDie':
        self.number_of_rolls += 1
        self.value = self.get_value()
        return self


class QuantumDie:

    def __init__(self):
        self.value = [1, 2, 3]

    def __repr__(self):
        return f"<QuantumDie>"

    def roll(self) -> 'QuantumDie':
        return self


class Position(Cycle):

    def __init__(self, initial_position):
        super().__init__(range(1, 11))
        self.position: Optional[int] = None
        self.move(number_of_places=initial_position)

    def __repr__(self):
        return f"<POSITION | {self.position}>"

    def move(self, number_of_places: int = 1) -> 'Position':
        self.position = super().get_value(steps=number_of_places)
        return self


class Player:

    def __init__(self, start_position: Position):
        self.position: Position = start_position
        self.points: int = 0

    def __repr__(self):
        return f"<PLAYER | {self.position}, {self.points}, {self.has_won()}>"

    def move(self, number_of_places: int):
        self.position.move(number_of_places)

    def add_points(self) -> None:
        self.points += self.position.position

    def has_won(self) -> bool:
        return self.points >= 21 # PART 2
        # return self.points >= 1000 # PART 1


class Players(Cycle):

    def __init__(self, players: List[Player]):
        self.players = players
        super().__init__(range(2))

    def __eq__(self, other):
        return str(self) == str(other)

    def __str__(self):
        return f"{self.players[0].position.position}-{self.players[0].points}+{self.players[1].position.position}-{self.players[1].points}"

    def __repr__(self):
        return f"<PLAYERS | {self.players}>"

    def __hash__(self):
        return hash(str(self))

    def __getitem__(self, index: int):
        return self.players[index]

    def get_next(self) -> int:
        """Get the next player and the index of this player"""
        index_of_player = super().get_value(steps=1)
        return index_of_player


class DiracDice:

    def __init__(self, dice, players: Players):
        self.dice = dice
        self.players: Players = players

    def __repr__(self):
        return f"<DIRACDICE | {self.dice}\n{self.players}>"

    def play(self):
        """Play the game until a player wins"""
        universes_won: List[int] = [0, 0]
        universes: Dict[Players, int] = {self.players: 1}

        while len(universes.keys()) > 0:
            print(len(universes.keys()))
            print(universes_won)

            current_player_index = self.players.get_next()
            for _ in range(3):
                new_universes: Dict[Players, int] = {}
                for universe, universe_count in universes.items():
                    values = self.dice.roll().value
                    for v in values:
                        to_store = deepcopy(universe)
                        to_store[current_player_index].move(number_of_places=v)
                        if to_store not in new_universes:
                            new_universes[to_store] = 0
                        new_universes[to_store] += universe_count
                universes = deepcopy(new_universes)

            new_universes: Dict[Players, int] = {}
            for universe, universe_count in universes.items():
                universe[current_player_index].add_points()
                if universe[current_player_index].has_won():
                    universes_won[current_player_index] += universe_count
                else:
                    new_universes[universe] = universe_count
            universes = deepcopy(new_universes)
        print(universes_won)
        print(f"PART 2 | The number of universes the player with most wins, wins in is {max(universes_won)}")

    def calculate_puzzle_result(self):
        # Multiply the number of dice rolls and the score of the losing player
        losing_player = self.players.get_next()
        print(f"PART 1 | The result of the number of dice rolls * score of losing player = {self.dice.number_of_rolls * losing_player.points}")


if __name__ == '__main__':
    input = read_input('input.txt')

    dd = DiracDice(
        dice=QuantumDie(),
        players=Players(players=[
            Player(start_position=Position(int(input[0][-1]))),
            Player(start_position=Position(int(input[1][-1]))),
        ])
    )

    dd.play()
