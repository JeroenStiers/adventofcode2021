from util import read_input

if __name__ == '__main__':
    input = [int(i) for i in read_input('input.txt')[0].split(',')]

    # define initial state of the list
    fishes = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for fish in input:
        fishes[fish] += 1

    # Simulate number of days
    for day in range(256):
        fishes.append(fishes.pop(0))
        fishes[6] += fishes[8]

    print(f"Number of fishes present: {sum(fishes)}")
