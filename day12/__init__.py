from enum import Enum
from typing import List, Dict
from util import read_input


class Size(Enum):
    SMALL = 1
    BIG = 2


class Cave:

    def __init__(self, name: str, size: Size):
        self.name: str = name
        self.size: Size = size
        self.connections = set()

    def __repr__(self):
        return f"<CAVE | '{self.name}' | {self.size} & {self.connections}>"

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(repr(self))

    @staticmethod
    def create_cave(name: str):
        return Cave(name=name,
                    size=Size.BIG if name.isupper() else Size.SMALL)

    def add_connection(self, name: str):
        self.connections.add(name)


class CaveSystem:

    def __init__(self):
        self.caves: Dict[str, Cave] = {}

    def __repr__(self):
        return '\n'.join([f"'{i[0]}': {i[1]}" for i in self.caves.items()])

    def interpret_connection(self, info: str):
        cave_from, cave_to = info.split('-')
        self.create_cave(cave_from)
        self.create_cave(cave_to)
        self.caves[cave_from].add_connection(cave_to)
        self.caves[cave_to].add_connection(cave_from)

    def create_cave(self, name: str):
        if name not in self.caves:
            self.caves[name] = Cave.create_cave(name)

    def generate_routes(self, cave_start_name: str, cave_end_name: str) -> List[List[str]]:
        routes: List[List[str]] = []
        self.__generate_route(existing_path=[cave_start_name], cave_end_name=cave_end_name, routes=routes)
        return routes

    def __generate_route(self, existing_path: List[str], cave_end_name: str, routes: List[List[str]]):
        current_cave = self.caves[existing_path[-1]]

        if current_cave == self.caves[cave_end_name]:
            # When we have reached the destination, return the path
            routes.append(existing_path)
            return

        for accessible_cave_name in current_cave.connections:
            accessible_cave = self.caves[accessible_cave_name]
            if not self.__is_path_valid(path=existing_path, cave=accessible_cave):
                # When we have reached a small cave that we have visited already
                # we are following a path that is not allowed. Therefore we will
                # do nothing more with this path
                pass
            else:
                # When this accessible cave is a valid option, add it to the path
                # and continue analysing the next steps
                updated_path = existing_path.copy()
                updated_path.append(accessible_cave.name)
                self.__generate_route(existing_path=updated_path,
                                      cave_end_name=cave_end_name,
                                      routes=routes)

    def __is_path_valid(self, path: List[str], cave: Cave):
        path = path.copy()
        path.append(cave.name)
        path = [self.caves[c] for c in path]

        if path.count(self.caves['start']) > 1 or path.count(self.caves['end']) > 1:
            # Start and end can only be visited exactly once
            return False

        small_caves_visited = []
        # Count for every small cave how many times it is visited
        for small_cave in set([c for c in path if c.size == Size.SMALL]):
            small_caves_visited.append(path.count(small_cave))

        # Validate how many small caves are visited twice - if this is more than 1, invalid path
        if small_caves_visited.count(2) > 1 or small_caves_visited.count(3) > 0:
            return False
        return True


if __name__ == '__main__':
    input = read_input('input.txt')

    # Build the cave system
    cave_system = CaveSystem()
    for i in input:
        cave_system.interpret_connection(i)
    print(cave_system)

    # Analyse the different paths to take
    routes = cave_system.generate_routes('start', 'end')
    # print(routes)
    print(f"{len(routes)} different pathways could be found")