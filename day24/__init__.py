from typing import List, Tuple, Dict, Union
from util import read_input
from collections import namedtuple
from math import floor
from copy import deepcopy
import re
from time import time, ctime

Instruction = namedtuple('Instruction', ['method', 'store_in', 'arguments'])


class ALU:

    def __init__(self):
        self.w = 0
        self.x = 0
        self.y = 0
        self.z = 0

    def __str__(self):
        return f"ALU: {self.w}, {self.x}, {self.y}, {self.z}"

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash((self.w, self.x, self.y, self.z))

    def __eq__(self, other: 'ALU'):
        return self.w == other.w and self.x == other.x and self.y == other.y and self.z == other.z

    def interpret_instruction(self, instruction: str):
        """Interpret the str as an instruction and add it to the self.instructions list
        to be used when running MONAD"""
        instruction_mapping = {
            'inp': self.__input,
            'add': self.__add,
            'mul': self.__multiply,
            'div': self.__division,
            'mod': self.__modulo,
            'eql': self.__equal
        }
        match = re.match(pattern=r'([a-z]{3}) ([wxyz]|[-0-9]+) ?([wxyz]|[-0-9]+)?', string=instruction)

        method = instruction_mapping[match.group(1)]
        store_in = match.group(2)
        arguments = [i for i in match.groups()[1:] if i]

        setattr(
            self,
            store_in,
            method(self.interpret_arguments(arguments))
        )
        return match.group(1)

    def validate_model_number(self, model_number: str) -> bool:
        """Reinitialise the ALU and run the validation of MONAD using the stored instructions"""
        # Initialise the ALU
        self.w, self.x, self.y, self.z = 0, 0, 0, 0
        self.model_number = [int(i) for i in model_number]

        for method, store_in, arguments in self.instructions:
            setattr(
                self,
                store_in,
                method(self.interpret_arguments(arguments))
            )

        return self.z == 0

    def interpret_arguments(self, arguments: List) -> List[int]:
        """Takes the input arguments and returns the interpreted values based on the
        current state of the ALU. Integers will stay integers but attributes (w, x, y, z) will have their
        current value returned"""
        to_return: List[int] = []
        for a in arguments:
            try:
                to_return.append(int(a))
            except ValueError:
                to_return.append(getattr(self, a))
        return to_return

    def __input(self, input: int) -> int:
        """Executes the input method and returns the result"""
        return input

    def __add(self, arguments: List[int]) -> int:
        """Adds both numbers togethers and returns the result"""
        return arguments[0] + arguments[1]

    def __multiply(self, arguments: List[int]) -> int:
        """multiply both numbers and returns result"""
        return arguments[0] * arguments[1]

    def __division(self, arguments: List[int]) -> int:
        """Divide both numbers and returns result"""
        return floor(arguments[0] / arguments[1])

    def __modulo(self, arguments: List[int]) -> int:
        """Calculate the module both numbers and returns result"""
        return arguments[0] % arguments[1]

    def __equal(self, arguments: List[int]) -> int:
        """Executes the equal method and returns the result"""
        return 1 if arguments[0] == arguments[1] else 0


class ALUSolver:

    def __init__(self, alu: ALU, instructions: List[str]):
        self.states: List[Tuple[str, ALU]] = [('93997999', alu)]
        self.valid_model_numbers: List[int] = []
        self.length_model_number: int = 8
        self.instructions: List[str] = instructions

    def solve(self):

        if self.length_model_number >= 14:
            for model_number, alu in self.states:
                if alu.z == 0:
                    self.valid_model_numbers.append(int(model_number))
            return

        while len(self.instructions) > 0:

            instruction = self.instructions[0]
            if instruction[0:3] == 'inp':
                break

            for model_number, alu in self.states:
                # Keep solving the next steps until we reach a new 'input' function where we will
                # analyse the current states, and branch for every digit
                if not alu.interpret_instruction(instruction=instruction):
                    break

            if instruction[0:3] == 'eql': # or (instruction[0:3] == 'mul' and instruction[-1] == '0'):
                self.branch(branch=False)

            # remove the instruction we just processed
            self.instructions.pop(0)

        try:
            self.branch()
            self.solve()
        except IndexError:
            return

    def branch(self, branch: bool = True):
        # Get the distinct states
        new_states: List[Tuple[str, ALU]] = []
        distinct_states: Dict[int, List[int, ALU]] = {}
        for s in self.states:
            state_hash = str(s[1])
            if state_hash not in distinct_states:
                distinct_states[state_hash] = [0, s[1]]
            try:
                distinct_states[state_hash][0] = max(distinct_states[state_hash][0], int(s[0]))
            except ValueError:
                distinct_states[state_hash][0] = ''

        if branch:
            instr = self.instructions.pop(0)
            store_in = instr[4:5]
            self.length_model_number += 1
            # for each distinct state, keep the maximum module_number linked to it
            for model_number, state in distinct_states.values():
                for i in range(9, 0, -1):
                    state_copy = deepcopy(state)
                    setattr(state_copy, store_in, i)
                    new_states.append((f"{str(model_number)}{i}", state_copy))
        else:
            for model_number, state in distinct_states.values():
                new_states.append((model_number, state))
        self.states = new_states


        print(f"{ctime(time())} | {self.length_model_number} - Number of distinct states: {len(self.states)}")


if __name__ == '__main__':
    input = read_input('input.txt')

    # Initialise and build the ALU
    alu = ALU()
    alu_solver = ALUSolver(alu, instructions=input)
    alu_solver.solve()
    print(alu_solver.valid_model_numbers)
    print(max(alu_solver.valid_model_numbers))

    exit(1)
    # Test for a given MONAD
    model_number = 99999999999999
    while not alu.validate_model_number(model_number=str(model_number)):

        # Generate the next model_number by subtracting 1 as long as there is a '0' present
        # in the model_number
        while True:

            if model_number % 100000 == 0:
                print(model_number)

            model_number -= 1
            if str(model_number).find('0') == -1:
                break

    print(model_number)
    print(alu.validate_model_number(model_number=str(model_number)))
    print(alu)

