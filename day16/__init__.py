from typing import List, Tuple, Dict, Optional, Union
from dataclasses import dataclass
from util import read_input, multiply
from math import ceil, floor


@dataclass
class Package:
    version: int
    id: int
    length_type_id: Optional[int]
    subpackage: Union[List['Package'], int]


class PackageAnalyser:

    def __init__(self, package: Package):
        self.package = package

    def sum_versions(self):
        return self.__sum_versions(self.package)

    def __sum_versions(self, package: Package):
        if isinstance(package.subpackage, int):
            return package.version

        elif isinstance(package.subpackage, list):
            versions_subpackages = 0
            for p in package.subpackage:
                versions_subpackages += self.__sum_versions(p)
            return versions_subpackages + package.version
        else:
            raise ValueError

    def work_out_value(self):
        return self.__work_out_value(self.package)

    def __work_out_value(self, package: Package):
        if package.id == 4:
            return package.subpackage

        values = []
        for p in package.subpackage:
            values.append(self.__work_out_value(p))

        if package.id == 0:
            return sum(values)
        if package.id == 1:
            return multiply(values)
        if package.id == 2:
            return min(values)
        if package.id == 3:
            return max(values)
        if package.id == 5:
            return 1 if values[0] > values[1] else 0
        if package.id == 6:
            return 1 if values[0] < values[1] else 0
        if package.id == 7:
            return 1 if values[0] == values[1] else 0

        raise ValueError


class BITSInterpreter:

    def translate_to_binary(self, input) -> str:
        sequence = f"{int(input, 16):04b}"

        # Calculate how many zeros to be added at the start
        count_to_add = ceil(len(sequence) / 4) * 4 - len(sequence)
        return f"{'0' * count_to_add}{sequence}"

    def parse(self, input: str):
        sequence = self.translate_to_binary(input)
        package, sequence = self.__parse_package(sequence)
        return package

    def __parse_package(self, sequence: str) -> Tuple[Package, str]:
        version, sequence = self.__fetch_version(sequence)
        id, sequence = self.__fetch_id(sequence)
        if id == 4:
            # In case of a 'literal value' package
            length_type_id = None
            subpackage, sequence = self.__fetch_subpackages_literal_value(sequence)

        else:
            # In case of an 'operator' package
            length_type_id, subpackage, sequence = self.__fetch_subpackages_operator(sequence)

        p = Package(
            version=version,
            id=id,
            length_type_id=length_type_id,
            subpackage=subpackage
        )

        return p, sequence

    @staticmethod
    def __fetch_version(seq: str) -> Tuple[int, str]:
        return int(seq[:3], 2), seq[3:]

    @staticmethod
    def __fetch_id( seq: str) -> Tuple[int, str]:
        return int(seq[:3], 2), seq[3:]

    @staticmethod
    def __fetch_subpackages_literal_value( seq: str) -> Tuple[int, str]:

        subpackage: str = ''
        while seq[0] == '1' and len(seq) >= 5:
            # Keep creating subpackages until the first digit of a 5bit string is a 0
            # This denotes the start of the last subpackage
            subpackage += seq[1:5]
            seq = seq[5:]

        return int(subpackage + seq[1:5], 2), seq[5:]

    def __fetch_subpackages_operator(self, seq: str) -> Tuple[int, List[int], str]:

        length_type_id = int(seq[0])
        seq = seq[1:]

        subpackages: List[int] = []

        if length_type_id == 0:
            number_of_bits = int(seq[:15], 2)
            seq = seq[15:]
            c = len(seq)
        elif length_type_id == 1:
            number_of_subpackages = int(seq[:11], 2)
            seq = seq[11:]
            c = number_of_subpackages
        else:
            raise ValueError

        while True:
            if length_type_id == 0 and (c - len(seq)) >= number_of_bits:
                break
            if length_type_id == 1:
                if c <= 0:
                    break
                c -= 1
            if seq == '':
                break
            package, seq = self.__parse_package(seq)
            subpackages.append(package)

        return length_type_id, subpackages, seq


if __name__ == '__main__':
    input = read_input('example.txt')

    interpreter = BITSInterpreter()
    package = interpreter.parse(input[0])

    print(package)

    pa = PackageAnalyser(package)
    print(f"PART 1 | Summed versions of packages: {pa.sum_versions()}")
    print(f"PART 2 | Value of the outermost package: {pa.work_out_value()}")
