from typing import List, Tuple, Dict
from util import read_input


if __name__ == '__main__':
    input = read_input('example.txt')
    print(input)