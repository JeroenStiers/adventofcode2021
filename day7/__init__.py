import math
from typing import List

from util import read_input


def define_fuel_per_step(to: int) -> List[int]:
    to_return = []

    for i in range(0, to+1):
        # to_return.append(i) # Part 1

        try:
            to_return.append(i+to_return[-1]) # Part 2
        except IndexError:
            to_return.append(0)
    return to_return


def define_optimal_position(initial_positions):

    fuel_per_step = define_fuel_per_step(max(initial_positions))
    print(fuel_per_step)

    fuel_per_position = []
    for position in range(min(initial_positions), max(initial_positions)+1, 1):
        fuel_needed = calculate_fuel_for_position(initial_positions, position, fuel_per_step)
        print(f"Position {position}: {fuel_needed}")
        fuel_per_position.append(fuel_needed)
    return fuel_per_position


def calculate_fuel_for_position(initial_positions: List[int], position: int, fuel_per_step: List[int]):
    return sum([fuel_per_step[abs(p-position)] for p in initial_positions])


if __name__ == '__main__':
    input = [int(i) for i in read_input('input.txt')[0].split(',')]

    fuel_per_position = define_optimal_position(input)
    print(min(fuel_per_position))
    print(f"Minimal fuel needed is {min(fuel_per_position)} which is for position {fuel_per_position.index(min(fuel_per_position))}")