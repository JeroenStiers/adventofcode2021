from typing import List
from util import read_input


def count_occurences(input: List[str]):
    input = [int(i) for i in input]
    return len(input) - sum(input), sum(input)


def keep_items_matching(input, index, value):
    result = []
    for i in input:
        if int(i[index]) == value:
            result.append(i)
    return result


def find_specific_value(input: List[str], preference: int, index: int = 0):

    if len(input) == 1:
        return input[0]

    other = 0 if preference == 1 else 1
    counts = count_occurences([i[index] for i in input])

    if (preference == 1 and counts[1] >= counts[0]) or (preference == 0 and counts[0] <= counts[1]):
        new_list = keep_items_matching(input, index, preference)
    else:
        new_list = keep_items_matching(input, index, other)

    return find_specific_value(new_list, preference, index+1)


if __name__ == '__main__':
    input = read_input('input.txt')
    input = [i for i in input if i != '']

    result_gamma = ''
    result_epsilon = ''
    for i in range(len(input[0])):
        count0, count1 = count_occurences([code[i] for code in input])

        if count1 > count0:
            result_gamma += '1'
            result_epsilon += '0'
        else:
            result_gamma += '0'
            result_epsilon += '1'

    print(f"{result_gamma} > {int(result_gamma, 2)}")
    print(f"{result_epsilon} > {int(result_epsilon, 2)}")
    print(f"Result: {int(result_gamma, 2) * int(result_epsilon, 2)}")

    # PART 2
    oxygen_generator_rating = find_specific_value(input, 1)
    co2_scrubber_rating = find_specific_value(input, 0)

    print(f"{oxygen_generator_rating} > {int(oxygen_generator_rating, 2)}")
    print(f"{co2_scrubber_rating} > {int(co2_scrubber_rating, 2)}")
    print(f"Result: {int(oxygen_generator_rating, 2) * int(co2_scrubber_rating, 2)}")


