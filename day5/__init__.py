import re
import itertools as it
from typing import Dict, Union, Tuple, List, Sequence, Iterator

from util import read_input


def generate_positions(input: Sequence) -> Iterator[Tuple[int, int]]:

    if input[0] == input[2]:
        from_y = min(int(input[1]), int(input[3]))
        to_y = max(int(input[1]), int(input[3]))
        y_values = range(from_y, to_y + 1, 1)
        return zip(it.repeat(int(input[0]), len(y_values)), y_values)
    elif input[1] == input[3]:
        from_x = min(int(input[0]), int(input[2]))
        to_x = max(int(input[0]), int(input[2]))
        x_values = range(from_x, to_x + 1, 1)
        return zip(x_values, it.repeat(int(input[1]), len(x_values)))
    else:
        # Diagonal lines
        from_x = int(input[0])
        to_x = int(input[2])
        from_y = int(input[1])
        to_y = int(input[3])

        if from_x > to_x:
            to_x -= 1
        else:
            to_x += 1

        if from_y > to_y:
            to_y -= 1
        else:
            to_y += 1

        return zip(range(from_x, to_x, -1 if from_x > to_x else 1), range(from_y, to_y, -1 if from_y > to_y else 1))


if __name__ == '__main__':
    input = read_input('input.txt')
    overview = {}

    try:
        pattern = r"(\d+),(\d+) -> (\d+),(\d+)"
        for i in input:
            positions = list(generate_positions(re.match(pattern, i).groups()))
            for position in positions:
                if position not in overview:
                    overview[position] = 0
                overview[position] += 1
    except AttributeError as e:
        print(f"\tIt seems that the regex did not find match.\n\tAssuming the overview is complete.\n\t{e}")

    dangerous_areas = []
    for position in overview.keys():
        if overview[position] >= 2:
            dangerous_areas.append(position)

    print(f"Number of dangerous areas: {len(dangerous_areas)}")



