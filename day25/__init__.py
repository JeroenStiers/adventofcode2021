from typing import List, Tuple, Dict
from util import read_input

WIDTH: int = 0
HEIGHT: int = 0


def new_position(pos, direction: str) -> Tuple[int, int]:

    if direction == 'south':
        if pos[0] + 1 == HEIGHT:
            return 0, pos[1]
        else:
            return pos[0] + 1, pos[1]

    if direction == 'east':
        if pos[1] + 1 == WIDTH:
            return pos[0], 0
        else:
            return pos[0], pos[1] + 1


def simulate_step(group_south: Dict[Tuple[int, int], str], group_east: Dict[Tuple[int, int], str]):

    group_south_new: Dict[Tuple[int, int], str] = {}
    group_east_new: Dict[Tuple[int, int], str] = {}
    number_moved: int = 0

    # define which cucumbers can move - first south
    for pos in group_east:
        new_pos = new_position(pos, 'east')
        if new_pos not in group_south and new_pos not in group_east:
            group_east_new[new_pos] = '>'
            number_moved += 1
        else:
            group_east_new[pos] = '>'

    # define which cucumbers can move - first south
    for pos in group_south:
        new_pos = new_position(pos, 'south')
        if new_pos not in group_south and new_pos not in group_east_new:
            group_south_new[new_pos] = 'v'
            number_moved += 1
        else:
            group_south_new[pos] = 'v'

    return group_south_new, group_east_new, number_moved


def print_state(group_south, group_east):
    for r in range(HEIGHT):
        for c in range(WIDTH):
            if (r, c) in group_south:
                print('v', end='')
            elif (r, c) in group_east:
                print('>', end='')
            else:
                print('.', end='')
        print('')
    print('')


if __name__ == '__main__':
    input = read_input('input.txt')

    WIDTH = len(input[0])

    group_south: Dict[Tuple[int, int], str] = {}
    group_east: Dict[Tuple[int, int], str] = {}

    for row in input:
        for c, j in enumerate(row):
            if j == '>':
                group_east[(HEIGHT, c)] = '>'
            elif j == 'v':
                group_south[(HEIGHT, c)] = 'v'
        HEIGHT += 1

    print(f"{HEIGHT} {WIDTH}")

    print_state(group_south, group_east)

    number_of_steps: int = 0
    while True:
        group_south, group_east, number_moved = simulate_step(group_south, group_east)
        number_of_steps += 1
        print(f'After step {number_of_steps}')
        #print_state(group_south, group_east)

        if number_moved == 0: # or number_of_steps == 4:
            break

    print(number_of_steps)

