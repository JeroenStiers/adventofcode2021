from copy import deepcopy
from typing import List, Tuple
from util import read_input
from PIL import Image, ImageDraw
from pathlib import Path


class Map:

    def __init__(self):
        self.grid: List[List[int]] = []
        self.size: Tuple[int, int] = (0, 0)
        self.step = 0

    def __repr__(self):
        for i in self.grid:
            print(''.join([str(j) for j in i]))
        return f"<MAP | {self.size}>"

    def add_row(self, input: str):
        self.grid.append([int(i) for i in input])
        self.size = len(self.grid), len(self.grid[0])

    def simulate_step(self) -> int:
        self.step += 1
        # Part 1 - Add 1 energy level to each position
        for r in range(self.size[0]):
            for c in range(self.size[1]):
                self.grid[r][c] += 1

        # Part 2 - Flash the octopi
        flashed: List[Tuple[int, int]] = []
        count_flashed_last_iteration: int = 1
        while count_flashed_last_iteration != 0:
            count_flashed_last_iteration = 0
            for r in range(self.size[0]):
                for c in range(self.size[1]):
                    if self.grid[r][c] > 9 and (r, c) not in flashed:
                        # When this position has reached the threshold but the octopus
                        # did not flash yet
                        flashed.append((r, c))
                        count_flashed_last_iteration += 1
                        self.flash_octopus(r, c)

        # Part 3 - Every octopus that flashed, receives 0 energy
        for f in flashed:
            self.grid[f[0]][f[1]] = 0
        return len(flashed)

    def flash_octopus(self, r: int, c: int):
        for al in self.define_adjacent_locations(r, c):
            self.grid[al[0]][al[1]] += 1

    def define_adjacent_locations(self, r: int, c: int) -> List[Tuple[int, int]]:
        adjacent_locations: List[Tuple[int, int]] = [
            (r - 1, c - 1),
            (r - 1, c),
            (r - 1, c + 1),
            (r, c - 1),
            (r, c + 1),
            (r + 1, c - 1),
            (r + 1, c),
            (r + 1, c + 1),
        ]

        # Only keep the locations within the map
        return [l for l in adjacent_locations
                if 0 <= l[0] < self.size[0]
                and 0 <= l[1] < self.size[1]]

    def get_total_energy(self):
        return sum([c for r in self.grid for c in r])

    def save_frame(self):
        img = Image.new('RGB', (self.size[1], self.size[0]))
        pixels = img.load()
        for ir, r in enumerate(self.grid):
            for ic, c in enumerate(r):
                pixels[ic, ir] = (236, 245, 66) if c == 0 else (20*c, 20*c, 20*c)
        img = img.resize(size=(img.size[0] * 50, img.size[1] * 50), resample=Image.BOX)
        # img.save(f'images/{self.step}.png')
        draw = ImageDraw.Draw(img)
        fill = (0, 0, 0) if pixels[0, 0][0] > 150 else (255, 255, 255)
        draw.text((5, 5), f"frame {self.step}", fill=fill)
        return img


if __name__ == '__main__':
    input = read_input('input.txt')

    map = Map()
    for row in input:
        map.add_row(row)

    map2 = deepcopy(map)

    flashed_per_step = []
    for i in range(100):
        flashed_per_step.append(map.simulate_step())

    print(f"PART 1 | Total number of flashing octopi: {sum(flashed_per_step)}")

    step = 0
    images = []
    while True:
        step += 1
        map2.simulate_step()
        images.append(map.save_frame())

        if map2.get_total_energy() == 0:
            break
    print(f"PART 2 | Total number of steps before all octopi flash together: {step}")

    # Generate a GIF
    images[0].save('gif.gif', save_all=True, duration=75, repeat=False, append_images=images[1:])