from enum import Enum
from typing import Set
from dataclasses import dataclass
from util import read_input
from PIL import Image, ImageDraw


@dataclass
class Position:
    x: int
    y: int

    def __repr__(self):
        return f"<Position {self.x},{self.y}>"

    def __hash__(self):
        return hash(repr(self))


class Char(Enum):
    EMPTY = 1
    DOT = 2

    def __repr__(self):
        return '#' if self == Char.DOT else ' '

    def __str__(self):
        return repr(self)


class Paper:

    def __init__(self, dot_positions: Set[Position]):
        self.dot_positions: Set[Position] = dot_positions

    def __repr__(self):
        return f"<PAPER | {self.dot_positions}>"

    def draw(self):
        count_rows = max([p.y for p in self.dot_positions]) + 1
        count_cols = max([p.x for p in self.dot_positions]) + 1
        for r in range(count_rows):
            row = []
            for c in range(count_cols):
                row.append(Char.DOT if Position(c, r) in self.dot_positions else Char.EMPTY)
            print(''.join([str(i) for i in row]))

    def fold(self, direction: str, index: int):
        if direction == 'fold along x':
            new_position_function = self.calculate_new_position_fold_left
        elif direction == 'fold along y':
            new_position_function = self.calculate_new_position_fold_up
        else:
            raise ValueError

        new_positions = set()
        for pos in self.dot_positions:
            new_x, new_y = new_position_function(pos=pos, fold_index=index)
            new_positions.add(Position(new_x, new_y))
        self.dot_positions = new_positions

    @staticmethod
    def calculate_new_position_fold_left(pos: Position, fold_index: int):
        if pos.x < fold_index:
            return pos.x, pos.y
        return pos.x - ((pos.x - fold_index) * 2), pos.y

    @staticmethod
    def calculate_new_position_fold_up(pos: Position, fold_index: int):
        if pos.y < fold_index:
            return pos.x, pos.y
        return pos.x, pos.y - ((pos.y - fold_index) * 2)

    def get_count_dots(self):
        return len(self.dot_positions)

    def save(self):
        count_rows = max([p.y for p in self.dot_positions]) + 1
        count_cols = max([p.x for p in self.dot_positions]) + 1
        img = Image.new('RGB', (count_cols, count_rows))
        pixels = img.load()
        for ir in range(count_rows):
            for ic in range(count_cols):
                pixels[ic, ir] = (0, 0, 0) if Position(ic, ir) in self.dot_positions else (255, 255, 255)
        img = img.resize(size=(img.size[0] * 20, img.size[1] * 20), resample=Image.NEAREST)
        img.save(f'result.png')


if __name__ == '__main__':
    input = read_input('input.txt')

    # Creating the initial paper
    dot_positions = []
    while i := input.pop(0):
        if i == '':
            break
        x, y = i.split(',')
        dot_positions.append(Position(int(x), int(y)))

    paper = Paper(set(dot_positions))

    # Start folding
    for fold in input:
        print(fold)
        direction, index = fold.split('=')
        paper.fold(direction, int(index))

    paper.draw()
    paper.save()
    print(paper.get_count_dots())






