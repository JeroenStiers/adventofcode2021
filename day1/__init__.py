from util import read_input


def generate_summed_windows(input, number_of_measurements: int):

    new_depths = []
    for i in range(0, len(input) - number_of_measurements + 1):
        new_depths.append(sum(input[i:i+number_of_measurements]))
    return new_depths


if __name__ == '__main__':
    input = read_input(filename='input.txt', convert_to_int=True)

    part1 = input
    part2 = generate_summed_windows(input, number_of_measurements=3)

    for part in [part1, part2]:
        count_increase = 0

        for index, value in enumerate(part):
            if index == 0:
                continue

            if value > part[index-1]:
                count_increase += 1

        print(part)
        print(count_increase)
