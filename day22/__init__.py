from itertools import product as it_product
from re import match as re_match
from typing import Tuple, TypeVar, Set

from util import read_input

Position = TypeVar(Tuple[int, int, int])


class Reactor:

    def __init__(self, procedure_region: dict[str, int]):
        self.cubes_turned_on: Set[Position] = set()
        self.procedure_region: dict[str, int] = procedure_region

    def act(self, input: str):
        m = re_match(r"^([a-z]{2,3}) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)", input)
        action, x_from, x_to, y_from, y_to, z_from, z_to = m.groups()

        for position in it_product(range(int(x_from), int(x_to) + 1), range(int(y_from), int(y_to) + 1), range(int(z_from), int(z_to) + 1)):
            if action == "on":
                self.cubes_turned_on.add(position)
            elif action == "off":
                self.cubes_turned_on.discard(position)

    def position_in_procedure_region(self, position: Position) -> bool:
        return self.procedure_region['x_from'] <= position[0] <= self.procedure_region['x_to'] \
                and self.procedure_region['y_from'] <= position[1] <= self.procedure_region['y_to'] \
                and self.procedure_region['z_from'] <= position[2] <= self.procedure_region['z_to']

    def return_cubes_in_procedure_region(self) -> Set[Position]:
        to_return: Set[Position] = set()
        for position in self.cubes_turned_on:
            if self.position_in_procedure_region(position):
                to_return.add(position)
        return to_return


if __name__ == '__main__':
    input = read_input('example.txt')

    r = Reactor(procedure_region={'x_from': -50, 'x_to': 50, 'y_from': -50, 'y_to': 50, 'z_from': -50, 'z_to': 50})
    for i in input[:]:
        print(i)
        r.act(i)

    print(len(r.return_cubes_in_procedure_region()))
