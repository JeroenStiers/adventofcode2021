import re

from typing import List
from dataclasses import dataclass

from util import read_input

@dataclass
class Command:
    action: str
    value: int


class CommandParser:

    def __init__(self):
        self.pattern = f'^([a-z]+) (\d+)'

    def parse_command(self, command: str):
        match = re.search(self.pattern, command, re.IGNORECASE)
        return Command(match.group(1), int(match.group(2)))


class Submarine:
    """Class to manage the specific properties of a submarine"""
    def __init__(self):
        self.horizontal_position = 0
        self.depth = 0
        self.aim = 0

    def __repr__(self):
        return f"<SUBMARINE | horizontal_position: {self.horizontal_position} & depth: {self.depth}>"

    def change_aim(self, amount: int):
        """Changes the aim of the submarine"""
        if not isinstance(amount, int):
            raise ValueError("The provided amount is not an integer.")
        self.aim += amount

    def change_depth(self, amount: int):
        """Moves the submarine in the vertical plane"""
        if not isinstance(amount, int):
            raise ValueError("The provided amount is not an integer.")
        self.depth += amount

    def change_horizontal_position(self, amount: int):
        """Moves the submarine in the horizontal plane"""
        if not isinstance(amount, int):
            raise ValueError("The provided amount is not an integer.")
        self.horizontal_position += amount


class Navigator:
    """Responsible to convert commands into movement actions for a given"""

    def __init__(self, submarine: Submarine):
        self.command_parser = CommandParser()
        self.submarine = submarine

    def navigate(self, command_list: List[str]):
        for command_str in command_list:
            command = self.command_parser.parse_command(command=command_str)

            if command.action == 'forward':
                self.submarine.change_horizontal_position(command.value)
                self.submarine.change_depth(command.value * self.submarine.aim)
            elif command.action == 'down':
                self.submarine.change_aim(command.value)
            elif command.action == 'up':
                self.submarine.change_aim(-1 * command.value)
            else:
                raise ValueError("The provided action '{command.action}' is not defined")


if __name__ == '__main__':
    input = read_input(filename='input.txt', convert_to_int=False)

    submarine = Submarine()
    navigator = Navigator(submarine=submarine)
    navigator.navigate(input)

    # Calculating the product of the horizontal and vertical position
    print(f"The submarine ({submarine}) generates a multiplied location of {submarine.horizontal_position * submarine.depth}")
