from math import floor
from typing import List
from util import read_input

CHARACTERS_OPEN = ['(', '[', '{', '<']
CHARACTERS_CLOSE = [')', ']', '}', '>']
ERROR_POINTS = [3, 57, 1197, 25137]
COMPLETION_POINTS = [1, 2, 3, 4]


def find_incorrect_closing(input: str):
    """Returns the first incorrect closing character if found any"""
    state = []
    for i in input:
        if i in CHARACTERS_OPEN:
            # Opening a new sequence is always allowed
            state.append(i)
        else:
            # Closing a sequence should be with the expected closing character
            if i != CHARACTERS_CLOSE[CHARACTERS_OPEN.index(state.pop())]:
                return i

    if len(state) != 0:
        # If incomplete, we return the opened chunks that are not closed
        return state
    return None


def calculate_completeness_score(opend_chunks: List[str]):

    completeness_score = 0
    try:
        while to_close := opend_chunks.pop():
            completeness_score = (completeness_score * 5) + COMPLETION_POINTS[CHARACTERS_OPEN.index(to_close)]
    except IndexError:
        return completeness_score


if __name__ == '__main__':
    input = read_input('input.txt')

    illegal_characters = []
    syntax_error_score = 0
    completeness_scores = []
    for i in input:
        char = find_incorrect_closing(i)
        if isinstance(char, str):
            illegal_characters.append(char)
            syntax_error_score += ERROR_POINTS[CHARACTERS_CLOSE.index(char)]
            continue

        # We now know we are not dealing with a corrupt string
        if isinstance(char, List):
            # If the input is incomplete, let's complete it
            completeness_scores.append(calculate_completeness_score(char))

    completeness_scores.sort()
    middle_completion_score = completeness_scores[floor(len(completeness_scores) / 2)]

    print(f"Part 1: The Syntax Error Score is {syntax_error_score}")
    print(f"Part 1: The middle Completion Score is {middle_completion_score}")