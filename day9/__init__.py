from typing import List, Tuple
from util import read_input, multiply


class Map:

    def __init__(self):
        self.grid: List[List[int]] = []
        self.size: Tuple[int, int] = (0, 0)

    def add_row(self, input: str):
        self.grid.append([int(i) for i in input])
        self.size = len(self.grid), len(self.grid[0])

    def find_low_points(self) -> List[Tuple[int, int]]:
        low_point_positions: List[Tuple[int, int]] = []
        for row in range(self.size[0]):
            for col in range(self.size[1]):
                height = self.grid[row][col]
                adjacent_locations = self.define_adjacent_locations(row, col)
                min_height = min([self.grid[l[0]][l[1]] for l in adjacent_locations])
                if min_height > height:
                    low_point_positions.append((row, col))
        return low_point_positions

    def define_adjacent_locations(self, r: int, c: int) -> List[Tuple[int, int]]:
        adjacent_locations: List[Tuple[int, int]] = [
            (r - 1, c),
            (r + 1, c),
            (r, c - 1),
            (r, c + 1)
        ]

        # Only keep the locations within the map
        return [l for l in adjacent_locations
                if 0 <= l[0] < self.size[0]
                and 0 <= l[1] < self.size[1]]

    def calculate_risk_level(self, positions: List[Tuple[int, int]]):
        return [self.grid[i[0]][i[1]] + 1 for i in positions]

    def calculate_basins(self, starting_locations: List[Tuple[int, int]]):
        basins = []
        for sl in starting_locations:
            r = []
            self.define_basin(sl, r)
            basins.append(r.copy())
        return basins

    def define_basin(self, location: Tuple[int, int], basin_locations: List[Tuple[int, int]]):
        if location in basin_locations:
            return

        basin_locations.append(location)
        adjacent_locations = self.define_adjacent_locations(location[0], location[1])
        for al in adjacent_locations:
            if self.grid[al[0]][al[1]] != 9 and self.grid[al[0]][al[1]] > self.grid[location[0]][location[1]]:
                self.define_basin(al, basin_locations)


if __name__ == '__main__':
    input = read_input('input.txt')

    map = Map()
    for row in input:
        map.add_row(row)

    low_points = map.find_low_points()
    risk_level = map.calculate_risk_level(low_points)
    print(f"PART 1 | The sum of the risk level on the map is: {sum(risk_level)}")

    basins = map.calculate_basins(low_points)
    basins = sorted(basins, key=lambda x: len(x), reverse=True)
    print(f"PART 2 | The size of the 3 largest basins multiplied together is: {multiply([len(b) for b in basins[:3]])}")