from pathlib import Path


def read_input(filename='input.txt', convert_to_int: bool = False):
    """
    Reads a text-files and returns every line in a list

    :param filename: the filename of the file to read (defaults to 'input.txt')
    :return: list containing all lines that are present in the file
    """
    with open(Path.cwd() / filename, 'r') as fid:
        lines = fid.read().splitlines()

    if convert_to_int:
        lines = [int(l) for l in lines]

    return lines


def multiply(numbers=[]):
    """
    Multiplies all numbers in the provided lists

    :param numbers: list of numbers to be multiplied
    :return: multiplied result
    """
    result = 1
    for number in numbers:
        result *= number
    return result


def define_groups(lines=[], separator="", concatenate_group=False):
    """
    Combines a list of features to groups based on a separtor. The resulting groups will be returned as a list of
    lists if concatenate_group = False or otherwise as a list of strings (different lines between one group are
    joined by a " "

    :param lines: list
    :param separator: character that defines a new group
    :param concatenate_group: boolean that indicates whether one group should consist of a string or a list itself
    :return:
    """
    result = []

    while len(lines) > 0:
        group = []
        while len(lines) > 0 and lines[0] != separator:
            group.append(lines[0])
            lines.pop(0)

        if len(lines) > 0:
            lines.pop(0)

        if concatenate_group:
            result.append(' '.join(group))
        else:
            result.append(group)

    return result
