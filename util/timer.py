from time import sleep, time


def time_function(func):
    """
    Function decorator used to measure running time.
    """

    def wrapper(*args, **kwargs):
        start = time()
        output = func(*args, **kwargs)
        end = time()

        print(f"{func.__name__} took {end - start} sec to run.")

        return output

    return wrapper


@time_function
def foo():
    sleep(1)  # do some "work"


if __name__ == "__main__":
    foo()