import re
from copy import deepcopy
from dataclasses import dataclass
from typing import List, Tuple, Dict, Union
from util import read_input

@dataclass
class Velocity:
    x: int
    y: int

    def update(self):
        self.y -= 1

        if self.x > 0:
            self.x -= 1
        if self.x < 0:
            self.x += 1

@dataclass
class Position:
    x: int
    y: int

    def update(self, velocity: Velocity):
        self.x += velocity.x
        self.y += velocity.y

@dataclass
class Trench:
    x_from: int
    x_to: int
    y_from: int
    y_to: int

    def is_passed(self, position: Position):
        return position.x > self.x_to or position.y < self.y_from

    def is_in(self, position: Position):
        return self.x_from <= position.x <= self.x_to and self.y_from <= position.y <= self.y_to


class Probe:

    def __init__(self, position: Position, trench: Trench):
        self.position = position
        self.trench = trench

    def define_x_velocity_range(self) -> Tuple[int, int]:
        # We can define the minimal x-velocity to reach the trench
        # formula to calculate distance given the velocity_x:
        # Distance = 0.5*velocity + 0.5*velocity^2
        # the maximal velocity is the distance in x from the probe to the trench_to
        # if the x-velocity would be higher we would overshoot
        max_range = self.trench.x_to - self.position.x
        min_range = self.trench.x_from - self.position.x

        velocity = 1
        while True:
            range = 0.5*velocity + 0.5*velocity**2

            if range >= min_range:
                return velocity, max_range+1
            velocity += 1

    def define_y_velocity_range(self) -> Tuple[int, int]:
        # The minimal velocity is defined by the distance of the probe to the bottom of the trench.
        # If it would be lower, we would overshoot in the first step.
        # Maximum is hardcoded.
        return self.trench.y_from - self.position.y, 250

    def reaches_trench(self, velocity: Velocity) -> Union[bool, Tuple[Position, int]]:
        position = deepcopy(self.position)
        velocity = deepcopy(velocity)

        steps_simulated = 0
        max_height = position.y
        while True:
            steps_simulated += 1
            # Update the position with the velocity
            position.update(velocity)
            max_height = max(max_height, position.y)

            if self.trench.is_in(position):
                # We have reached the trench
                return position, max_height
            if self.trench.is_passed(position):
                # We can never reach the trench
                return False

            # update the velocity
            velocity.update()


if __name__ == '__main__':
    input = read_input('input.txt')[0]
    match = re.search('x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)', input)

    # Generate the trench data
    x_from, x_to, y_from, y_to = match.groups()
    trench = Trench(int(x_from), int(x_to), int(y_from), int(y_to))

    # Initialise the probe
    probe = Probe(position=Position(0, 0), trench=trench)

    # Define the range of velocities to test
    minimal_velocity_x, maximal_velocity_x = probe.define_x_velocity_range()
    minimal_velocity_y, maximal_velocity_y = probe.define_y_velocity_range()

    # For each of the possible x-velocities, try a bunch of y-velocities to figure out
    # if the probe reaches the trench and what the maximal height it reached was
    valid_configurations = []
    for velocity_x in range(minimal_velocity_x, maximal_velocity_x+1, 1):
        for velocity_y in range(minimal_velocity_y, maximal_velocity_y+1, 1):
            # TODO - The code can be optimised to limit the velocities to try
            # We could optimise here - for this x-velocity, check for which y-velocity we will
            # overshoot (as in reach a position.x that is > trench.x_to and position.y > position.y_to.
            # Now we can limit the maximal_y velocity to this velocity since a higher velocity_x will
            # lead to overshoot faster (as in with a lower velocity_y
            velocity = Velocity(velocity_x, velocity_y)
            if (result := probe.reaches_trench(velocity)) is not False:
                valid_configurations.append((velocity, result[0], result[1]))

    # from pprint import pprint
    # pprint(valid_configurations)

    print(f"PART 1 | Maximum height reached: {max([i[2] for i in valid_configurations])}")
    print(f"PART 2 | Number of possible start velocities: {len(valid_configurations)}")

