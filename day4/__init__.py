import re
from typing import List, Union
from dataclasses import dataclass
from util import read_input


@dataclass
class Dimension:
    rows: int
    cols: int


@dataclass
class WinningBoard:
    board: 'BingoBoard'
    drawn_numbers: List[int]


class BingoBoard:
    """Class to keep track of a Bingo Board"""
    def __init__(self):
        self.id: int = None
        self.__pattern = r'(\d+)'
        self.numbers: List[int] = []
        self.dimensions: Dimension = Dimension(0, 0)

    def __repr__(self):
        return f"<BINGOBOARD | {self.dimensions} {self.numbers}>"

    def __eq__(self, other):
        return self.id == other.id

    def add_row(self, row: str) -> None:
        """Clean the input, convert to numbers and add them to self.numbers."""
        to_add: List[int] = []
        matches = re.findall(self.__pattern, row)
        for n in matches:
            to_add.append(int(n))
        self.numbers.extend(to_add)

        # Update the dimensions of the board
        self.dimensions.rows += 1
        self.dimensions.cols = len(to_add)

    def has_won(self, drawn_numbers: List[int]) -> bool:
        """Calculates if this board has won.
        :param: numbers - Contains a list of the already drawn numbers"""
        drawn_numbers = set(drawn_numbers)
        # Run through each row and validate if this row is complete
        for r in range(self.dimensions.rows):
            if len(drawn_numbers.intersection(set(self.numbers[r * self.dimensions.cols : (r + 1) * self.dimensions.cols]))) == self.dimensions.cols:
                return True

        modulo = [i % self.dimensions.rows for i in range(len(self.numbers))]
        # Run through each column and validate if it is complete
        for c in range(self.dimensions.cols):
            if len(drawn_numbers.intersection(set([j for i, j in zip(modulo, self.numbers) if i == c]))) == self.dimensions.rows:
                return True
        return False


class BingoGame:
    """Class keeping track of the BingoBoards"""
    def __init__(self):
        self.drawn_numbers: List[int] = []
        self.boards: List[BingoBoard] = []
        self.winning_boards: List[WinningBoard] = []

    def __repr__(self):
        return f"<BINGOGAME | {self.drawn_numbers} {self.boards}>"

    def add_board(self, board: BingoBoard):
        self.boards.append(board)
        board.id = len(self.boards)

    def draw_number(self, number: int):
        self.drawn_numbers.append(number)
        return True

    def check_boards(self):
        for board in self.boards:
            if board in [b.board for b in self.winning_boards]:
                continue
            if board.has_won(self.drawn_numbers):
                self.winning_boards.append(WinningBoard(board, self.drawn_numbers.copy()))


if __name__ == '__main__':
    # SETUP
    input = read_input('input.txt')

    numbers_to_draw = [int(i) for i in input[0].split(',')]
    game = BingoGame()

    # Creates the different boards
    for line in input[1:]:

        if line == '':
            board = BingoBoard()
            game.add_board(board)
            continue
        board.add_row(line)


    # Plays the game until a board wins or all numbers are drawn.
    try:
        while game.draw_number(numbers_to_draw.pop(0)):
            game.check_boards()
    except IndexError:
        pass

    if len(game.winning_boards) == 0:
        # It seems that there is no winner
        print("Nobody wins :(")
    else:
        winning_board = game.winning_boards[0] # part 1
        winning_board = game.winning_boards[-1] # part 2

        print(f"Winning board: {winning_board.board}")
        print(f"Number drawn to win: {winning_board.drawn_numbers[-1]}")
        sum_unmarked_numbers = sum(set(winning_board.board.numbers).difference(set(winning_board.drawn_numbers)))
        print(f"Sum of unmarked numbers: {sum_unmarked_numbers}")
        print(f"Result: {winning_board.drawn_numbers[-1] * sum_unmarked_numbers}")