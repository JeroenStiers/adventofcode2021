"""
    Information / tutorial followed:
    https://www.redblobgames.com/pathfinding/a-star/implementation.html

"""

import heapq
from copy import deepcopy
from itertools import cycle
from timeit import default_timer as timer
from typing import List, Tuple, Dict, Iterator, Optional

from util import read_input

GridLocation = Tuple[int, int]


class PriorityQueue:
    def __init__(self):
        self.elements: List[Tuple[int, GridLocation]] = []

    def empty(self) -> bool:
        return not self.elements

    def put(self, item: GridLocation, priority: int):
        heapq.heappush(self.elements, (priority, item))

    def get(self) -> GridLocation:
        return heapq.heappop(self.elements)[1]


class SquareGrid:
    def __init__(self):
        self.width = 0
        self.height = 0
        self.grid: List[List[int]] = []

    def build_grid(self, input: str):
        self.grid.append([int(i) for i in input])
        self.width = len(self.grid[0])
        self.height = len(self.grid)

    def in_bounds(self, id: GridLocation) -> bool:
        (x, y) = id
        return 0 <= x < self.height and 0 <= y < self.width

    def passable(self, id: GridLocation) -> bool:
        return True

    def neighbours(self, id: GridLocation) -> Iterator[GridLocation]:
        (x, y) = id
        neighbors = [(x + 1, y), (x - 1, y), (x, y - 1), (x, y + 1)]  # E W N S
        if (x + y) % 2 == 0: neighbors.reverse()  # S N W E
        results = filter(self.in_bounds, neighbors)
        results = filter(self.passable, results)
        return results

    def expand(self, count: int):
        # Horizontal expansion
        new_grid: List[List[int]] = deepcopy(self.grid)
        for iteration in range(count):
            for ir, r in enumerate(self.grid):
                new_grid[ir].extend([self.__repeat_value(i, iteration+1) for i in r])

        # Vertical expansion
        self.grid = deepcopy(new_grid)
        for iteration in range(count):
            for r in self.grid:
                new_grid.append([self.__repeat_value(i, iteration+1) for i in r])
        self.grid = new_grid
        self.width = len(self.grid[0])
        self.height = len(self.grid)

    def __repeat_value(self, initial_value: int, steps: int) -> int:
        c = cycle([1, 2, 3, 4, 5, 6, 7, 8, 9])
        while next(c) != initial_value:
            pass

        new_value = initial_value
        for _ in range(steps):
            new_value = next(c)
        return new_value

    def draw(self):
        for r in self.grid:
            print(''.join([str(c) for c in r]))
        print()


class WeightedSquareGrid(SquareGrid):
    def __init__(self):
        super().__init__()

    def cost(self, from_node: GridLocation, to_node: GridLocation) -> int:
        r, c = to_node
        return self.grid[r][c]


def heuristic(a: GridLocation, b: GridLocation) -> int:
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)


def a_start_search(graph: WeightedSquareGrid, start: GridLocation, goal: GridLocation) -> int:
    frontier = PriorityQueue()
    frontier.put(item=start, priority=0)
    came_from: Dict[GridLocation, Optional[GridLocation]] = {start: None}
    cost_so_far: Dict[GridLocation, int] = {start: 0}

    test_count = 0
    while not frontier.empty():
        current: GridLocation = frontier.get()

        if current == goal:
            break

        for next in graph.neighbours(current):
            test_count += 1
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                frontier.put(next, new_cost + heuristic(next, goal))
                came_from[next] = current
    print(test_count)
    return came_from, cost_so_far


def reconstruct_path(came_from: Dict[GridLocation, GridLocation],
                     start: GridLocation, goal: GridLocation) -> List[GridLocation]:
    current: GridLocation = goal
    path: List[GridLocation] = []
    while current != start:
        path.append(current)
        current = came_from[current]
    return path


if __name__ == '__main__':
    input = read_input('input.txt')

    grid = WeightedSquareGrid()

    for row in input:
        grid.build_grid(row)
    grid.expand(4)

    start = (0, 0)
    goal = (grid.height-1, grid.width-1)

    start_time = timer()
    came_from, cost_so_far = a_start_search(grid, start=start, goal=goal)
    path = reconstruct_path(came_from, start=start, goal=goal)
    end_time = timer()
    print(f"Total cost: {cost_so_far[goal]}")
    print(end_time - start_time)
