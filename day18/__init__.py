import re
from dataclasses import dataclass
from math import floor, ceil
from copy import deepcopy
from typing import Tuple, Union, Optional
from util import read_input
from itertools import permutations


@dataclass
class SFComponent:
    left: Optional[Union[int, 'SFComponent']]
    right: Optional[Union[int, 'SFComponent']]

    def __str__(self):
        return f"[{self.left},{self.right}]"

    def __repr__(self):
        return f"[{self.left},{self.right}]"

    def __eq__(self, other: 'SFComponent') -> bool:
        return str(self) == str(other)


class SFNumber:

    def __init__(self, component: SFComponent):
        self.component: SFComponent = component

    def __repr__(self):
        return f"<SFNUMBER | {self.component}>"

    def __add__(self, other: 'SFNumber'):
        return SFNumber(SFNumber.reduce(SFComponent(left=self.component, right=other.component)))

    @staticmethod
    def calculate_magnitude(component: SFComponent) -> int:
        result = 0

        if isinstance(component.left, SFComponent):
            result += SFNumber.calculate_magnitude(component.left) * 3
        else:
            result += component.left * 3

        if isinstance(component.right, SFComponent):
            result += SFNumber.calculate_magnitude(component.right) * 2
        else:
            result += component.right * 2

        return result

    @staticmethod
    def reduce(component: SFComponent) -> SFComponent:
        while True:
            new_component = SFNumber.explode(deepcopy(component))
            if new_component != component:
                # print(f"     EXPLODE: {component}\n            > {new_component}")
                component = new_component
                continue

            new_component = SFNumber.split(deepcopy(component))[0]
            if new_component != component:
                # print(f"       SPLIT: {component}\n            > {new_component}")
                component = new_component
                continue

            return new_component

    @staticmethod
    def explode(component: SFComponent) -> SFComponent:
        to_add_left, to_add_right, location_explosion, _ = SFNumber.find_information_explosion(component)
        if location_explosion is not None:
            return SFNumber.update_component_after_exploding(component, to_add_left, to_add_right, location_explosion)[1]
        return component

    @staticmethod
    def find_information_explosion(component: SFComponent, level: int = 0, current_location: int = 0) -> Tuple[int, int, int, int]:

        to_add_left: int = 0
        to_add_right: int = 0
        location_of_explosion = None

        if level == 4:
            # Let's explode this component
            return component.left, component.right, current_location, current_location

        if isinstance(component.left, SFComponent) and location_of_explosion is None:
            to_add_left, to_add_right, location_of_explosion, current_location = SFNumber.find_information_explosion(component.left, level + 1, current_location)
        else:
            current_location += 1

        if isinstance(component.right, SFComponent) and location_of_explosion is None:
            to_add_left, to_add_right, location_of_explosion, current_location = SFNumber.find_information_explosion(component.right, level + 1, current_location)
        else:
            current_location += 1

        return to_add_left, to_add_right, location_of_explosion, current_location

    @staticmethod
    def update_component_after_exploding(component: SFComponent, to_add_left: int, to_add_right: int, location_of_explosion: int, current_location: int = 0) -> Tuple[int, SFComponent]:

        if current_location == location_of_explosion and isinstance(component.left, int) and isinstance(component.right, int):
            return current_location+1, 0

        if isinstance(component.left, SFComponent):
            current_location, component.left = SFNumber.update_component_after_exploding(component.left, to_add_left, to_add_right, location_of_explosion, current_location)
        else:
            if current_location == location_of_explosion - 1:
                component.left += to_add_left
            elif current_location == location_of_explosion + 1:
                component.left += to_add_right
            current_location += 1

        if isinstance(component.right, SFComponent):
            current_location, component.right = SFNumber.update_component_after_exploding(component.right, to_add_left, to_add_right, location_of_explosion, current_location)
        else:
            if current_location == location_of_explosion - 1:
                component.right += to_add_left
            elif current_location == location_of_explosion + 1:
                component.right += to_add_right
            current_location += 1

        return current_location, SFComponent(component.left, component.right)

    @staticmethod
    def split(component: SFComponent, can_split: bool = True) -> Tuple[SFComponent, bool]:

        if not can_split:
            # If we cannot split anymore, just return the current state
            return component, can_split

        # Check if the left component is splittable, if not, we check the right component
        if can_split and isinstance(component.left, int) and component.left >= 10:
            component.left = SFComponent(left=floor(component.left / 2), right=ceil(component.left / 2))
            return component, False

        if isinstance(component.left, SFComponent):
            component.left, can_split = SFNumber.split(component.left, can_split)

        if can_split and isinstance(component.right, int) and component.right >= 10:
            component.right = SFComponent(left=floor(component.right / 2), right=ceil(component.right / 2))
            return component, False

        if isinstance(component.right, SFComponent):
            component.right, can_split = SFNumber.split(component.right, can_split)

        return component, can_split

    @staticmethod
    def parse(input: str) -> 'SFNumber':
        # Go over all characters and create a different SFComponent for each of the opening
        # brackets found.
        return SFNumber(SFNumber.parse_component(input[1:-1])[0])

    @staticmethod
    def parse_component(sequence: str) -> Tuple[SFComponent, str]:
        left: Union[int, SFComponent]
        right: Union[int, SFComponent]

        if re.match('\d', sequence[0]):
            left = int(sequence[0])
            sequence = sequence[2:]
        elif sequence[0] == '[':
            left, sequence = SFNumber.parse_component(sequence[1:])
        else:
            raise ValueError(sequence)

        # If we get here, the left component is built and we can move on to the right component
        if re.match('\d', sequence[0]):
            right = int(sequence[0])
            sequence = sequence[2:]
        elif sequence[0] == '[':
            right, sequence = SFNumber.parse_component(sequence[1:])

        return SFComponent(left, right), sequence[1:]


if __name__ == '__main__':

    # # TESTS #
    # print(f"Running some tests:")
    # input = read_input('test_explode.txt')
    # for i in input[:]:
    #     sfn = SFNumber.parse(i)
    #     component = sfn.explode(sfn.component)
    #     print(f"{'EXPLODE:':<10} {i} --> {component}")
    #
    # input = read_input('test_magnitude.txt')
    # for i in input[:]:
    #     sfn = SFNumber.parse(i)
    #     magnitude = SFNumber.calculate_magnitude(sfn.component)
    #     print(f"{'MAGNITUDE:':<10} {i} --> {magnitude}")

    ## REAL CODE ##
    print(f"\nHelping the smaller SnailFish")
    input = read_input('input.txt')
    result = SFNumber.parse(input[0])
    for i in input[1:]:
        sfn = SFNumber.parse(i)
        new_result = result + sfn
        # print(f"  {result}\n+ {sfn}\n= {new_result}\n")
        result = new_result

    print(f"PART 1 | Result of the complete sum is:\n{result}")
    magnitude = SFNumber.calculate_magnitude(result.component)
    print(f"PART 1 | The magnitude of this som is :\n{magnitude}")

    max_magnitude = 0
    for option in permutations(input, 2):
        result = SFNumber.parse(option[0]) + SFNumber.parse(option[1])
        magnitude = SFNumber.calculate_magnitude(result.component)

        if magnitude > max_magnitude:
            max_magnitude = magnitude

    print(f"PART 2 | The maximum magnitude of the combinations is :\n{max_magnitude}")


