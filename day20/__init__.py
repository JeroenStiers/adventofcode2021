from typing import List, Tuple, Dict
from copy import deepcopy
from util import read_input


class InfinateImage:

    def __init__(self, enhancement_algorithm: str):
        self.grid: List[List[str]] = []
        self.enhancement_algorithm: List[str] = ['0' if i == '.' else '1' for i in enhancement_algorithm]
        self.size: Dict[str, int] = {'height': 0, 'width': 0}
        self.times_enhanced = 0
        self.completion_char = [self.enhancement_algorithm[-1], self.enhancement_algorithm[0]]

    def __repr__(self):
        for i in self.grid:
            print(''.join([str(j) for j in i]))
        return f"<InfinateImage | {self.size}\n{self.enhancement_algorithm}>"

    def add_row(self, input: str):
        self.grid.append(['0' if i == '.' else '1' for i in input])
        self.size = {'height': len(self.grid), 'width': len(self.grid[0])}

    def enhance(self):
        """
        1. Infinitise the image with 2 extra rows and columns at the all boundaries so that we can
        fake an infinate image.
        2. Run the enhancement algortihm on all indices [1:-1] (not the boundaries)
        3. Run infinitise with 1 extra row and column to return an image of a similar size as the one
        befire the enhancement
        """
        ii.infinitise(number_of_steps=2)
        ii.run_enhancement_algorithm()
        # ii.infinitise(number_of_steps=1)
        # print(self)

    def infinitise(self, number_of_steps: int = 1):
        if self.times_enhanced == 0:
            completion_char = '0'
        else:
            completion_char = self.completion_char[self.times_enhanced % 2]
        new_grid: List[List[str]] = []
        # Make the image wider
        for index_row, row in enumerate(self.grid):
            to_add_to_row = [completion_char for _ in range(number_of_steps)]
            new_row: List[str] = deepcopy(to_add_to_row)
            new_row.extend(row)
            new_row.extend(to_add_to_row)
            new_grid.append(new_row)
        self.size['width'] = len(new_grid[0])

        # Make the image higher
        extra_row = [completion_char for _ in range(self.size['width'])]

        for _ in range(number_of_steps):
            new_grid.insert(0, extra_row)
        for _ in range(number_of_steps):
            new_grid.append(extra_row)
        self.grid = new_grid
        self.size['height'] = len(new_grid)

    def run_enhancement_algorithm(self):
        new_grid: List[List[str]] = []
        for ir in range(1, self.size['height']-1):
            new_row: List[str] = []
            for ic in range(1, self.size['width']-1):
                index = self.define_index(ir, ic)
                new_row.append(self.enhancement_algorithm[index])
            new_grid.append(new_row)
        self.grid = new_grid
        self.size = {'height': len(self.grid), 'width': len(self.grid[0])}
        self.times_enhanced += 1

    def count_lit_pixels(self):
        number_of_lit_pixels: int = 0
        for row in self.grid:
            number_of_lit_pixels += sum([int(i) for i in row])
        return number_of_lit_pixels

    def define_index(self, r: int, c: int) -> int:
        try:
            binary = self.grid[r-1][c-1] + self.grid[r-1][c] + self.grid[r-1][c+1]
            binary += self.grid[r][c-1] + self.grid[r][c] + self.grid[r][c+1]
            binary += self.grid[r+1][c-1] + self.grid[r+1][c] + self.grid[r+1][c+1]
            return int(binary, 2)
        except IndexError as e:
            print(self)
            print(r)
            print(c)
            raise e


if __name__ == '__main__':
    input = read_input('input.txt')

    ii = InfinateImage(enhancement_algorithm=input[0])
    for i in input[2:]:
        ii.add_row(i)

    images = []
    images.append(ii.save_frame())
    for step in range(50):
        print(f"Performing enhancement {step}")
        ii.enhance()
        images.append(ii.save_frame())

    print(f"PART 1 & 2 | The number of lit pixels: {ii.count_lit_pixels()}")